package com.bcp.exchangerate.security.domain;

import java.io.Serializable;

public class JwtResponse implements Serializable {

    private static final long serialVersionUID = -1L;

    private final String jwtToken;

    public JwtResponse(String token){
        this.jwtToken = token;
    }

    public String getJwtToken(){
        return this.jwtToken;
    }
}
