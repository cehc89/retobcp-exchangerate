package com.bcp.exchangerate.service;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Definicion usuario y contraseñas
 */
@Service
public class JwtUserDetailsService implements UserDetailsService {

    //Usuarios de prueba nombre/password encriptado con BCcryp (el pass es el mismo que el usuario)
    Map<String, String> users = new HashMap<String,String>(){
        {
            put("carlos", "$2a$12$X/QnVHJICJAVflASJFkqHu.FXlVXRfWd9iIvI7sfXXDmJzM1AwYAu");
            put("user", "$2a$12$inElDy3JFwMoK9v7O7JVvOoXO1FDQcmOYvvGioSBwmZ3ujjwxC2B2");
        }
    };

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        if(users.get(s)!=null){
            return new User(s, users.get(s), new ArrayList<>());
        } else {
            throw new UsernameNotFoundException("Usuario no encontrado");
        }
    }
}
