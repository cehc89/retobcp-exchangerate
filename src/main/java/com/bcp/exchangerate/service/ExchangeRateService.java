package com.bcp.exchangerate.service;

import com.bcp.exchangerate.service.dto.AskExchangeRateDTO;
import com.bcp.exchangerate.service.dto.ExchangeRateDTO;
import io.reactivex.Completable;
import io.reactivex.Single;

import java.math.BigDecimal;
import java.util.List;

public interface ExchangeRateService {

    Single<Integer> addExchangeRate(ExchangeRateDTO exchangeRate);

    Completable updateRate(ExchangeRateDTO exchangeRate);

    Single<List<ExchangeRateDTO>> getAllRatesFor(String base);

    Single<ExchangeRateDTO> getRateFor(String base, String target);

    BigDecimal doChange(String base, String target, BigDecimal amount) throws Exception;
}
