package com.bcp.exchangerate.service.dto;

import com.bcp.exchangerate.datamodel.ExchangeRateType;

import java.math.BigDecimal;

public class ExchangeAmountDTO {

    private String currencyBase;
    private BigDecimal amountBase;
    private String currencyTarget;
    private BigDecimal amountTarget;
    private ExchangeRateType type;

    public String getCurrencyBase() {
        return currencyBase;
    }

    public void setCurrencyBase(String currencyBase) {
        this.currencyBase = currencyBase;
    }

    public BigDecimal getAmountBase() {
        return amountBase;
    }

    public void setAmountBase(BigDecimal amountBase) {
        this.amountBase = amountBase;
    }

    public String getCurrencyTarget() {
        return currencyTarget;
    }

    public void setCurrencyTarget(String currencyTarget) {
        this.currencyTarget = currencyTarget;
    }

    public BigDecimal getAmountTarget() {
        return amountTarget;
    }

    public void setAmountTarget(BigDecimal amountTarget) {
        this.amountTarget = amountTarget;
    }

    public ExchangeRateType getType() {
        return type;
    }

    public void setType(ExchangeRateType type) {
        this.type = type;
    }
}
