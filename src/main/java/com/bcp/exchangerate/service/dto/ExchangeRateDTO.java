package com.bcp.exchangerate.service.dto;

import com.bcp.exchangerate.datamodel.ExchangeRate;

import org.springframework.beans.BeanUtils;
import org.springframework.data.jpa.util.BeanDefinitionUtils;

import java.math.BigDecimal;
import java.time.LocalDate;

public class ExchangeRateDTO {

    private Integer id;
    private String base;
    private String target;
    private BigDecimal rate;
    private LocalDate date;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public ExchangeRate parseToEntity(){
        ExchangeRate rate = new ExchangeRate();
        BeanUtils.copyProperties(this,rate);
        return rate;
    }

    public static ExchangeRateDTO parseToDTO(ExchangeRate rate){
        ExchangeRateDTO dto = new ExchangeRateDTO();

        BeanUtils.copyProperties(rate,dto);
        return dto;
    }
}
