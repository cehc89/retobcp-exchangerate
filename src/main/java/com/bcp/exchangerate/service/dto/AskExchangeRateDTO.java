package com.bcp.exchangerate.service.dto;

import java.time.LocalDate;

public class AskExchangeRateDTO {

    private String base;
    private String target;
    private LocalDate date;

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
