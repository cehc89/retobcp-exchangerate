package com.bcp.exchangerate.service;

import com.bcp.exchangerate.datamodel.ExchangeRate;
import com.bcp.exchangerate.repository.ExchangeRateRepository;
import com.bcp.exchangerate.service.dto.ExchangeRateDTO;
import io.reactivex.Completable;
import io.reactivex.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ExchangeRateServiceImpl implements ExchangeRateService{

    @Autowired
    private ExchangeRateRepository exchangeRateRepo;

    @Override
    public Single<Integer> addExchangeRate(ExchangeRateDTO exchangeRate) {
        if(exchangeRate.getDate()==null)
            exchangeRate.setDate(LocalDate.now());
        return Single.create(subscriber -> {
            Integer newID = exchangeRateRepo.save(exchangeRate.parseToEntity()).getId();
            subscriber.onSuccess(newID);
        });
    }

    @Override
    public Completable updateRate(ExchangeRateDTO exchangeRate){
        return Completable.create(subscriber -> {
            Optional<ExchangeRate> rate = exchangeRateRepo.findById(exchangeRate.getId());
            if(!rate.isPresent())
                subscriber.onError(new EntityNotFoundException());
            else{
                ExchangeRate rateToUpdate = rate.get();
                rateToUpdate.setRate(exchangeRate.getRate());
                exchangeRateRepo.save(rateToUpdate);
                subscriber.onComplete();
            }
        });
    }

    @Override
    public Single<List<ExchangeRateDTO>> getAllRatesFor(String base) {
        return Single.create(subscriber -> {
            List<ExchangeRate> rates = exchangeRateRepo.listAllRatesFor(base);
            List<ExchangeRateDTO> ratesDTO = rates.stream().map(rate -> ExchangeRateDTO.parseToDTO(rate)).collect(Collectors.toList());
            subscriber.onSuccess(ratesDTO);
        });
    }

    @Override
    public Single<ExchangeRateDTO> getRateFor(String base, String target) {
        return Single.create(subscriber -> {
            ExchangeRate rate = exchangeRateRepo.getRateFor(base,target);
            subscriber.onSuccess(ExchangeRateDTO.parseToDTO(rate));
        });
    }

    @Override
    public BigDecimal doChange(String base, String target, BigDecimal amount) throws Exception{
        ExchangeRate rate = exchangeRateRepo.getRateFor(base,target);
        if(rate == null)
            throw new Exception("No se encontro tasa");
        return amount.divide(rate.getRate(),2, RoundingMode.HALF_UP);
    }

}
