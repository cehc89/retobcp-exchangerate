package com.bcp.exchangerate.controller.response;

/**
 * Objeto base de respuesta REST {errorCode, data}
 * @param <T>
 */
public class BaseResponse<T> {

    private String errorCode;
    private T data;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static <T> BaseResponse okWithData(T data){
        BaseResponse res = new BaseResponse<T>();
        res.data = data;
        return res;
    }

    public static BaseResponse okWithoutData(){
        return new BaseResponse();
    }

    public static BaseResponse error(String errorCode){
        BaseResponse res = new BaseResponse();
        res.errorCode = errorCode;
        return res;
    }
}
