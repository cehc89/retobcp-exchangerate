package com.bcp.exchangerate.controller;

import com.bcp.exchangerate.controller.response.BaseResponse;
import com.bcp.exchangerate.controller.response.ExchangeRateResponse;
import com.bcp.exchangerate.service.ExchangeRateService;
import com.bcp.exchangerate.service.dto.ExchangeAmountDTO;
import com.bcp.exchangerate.service.dto.ExchangeRateDTO;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.net.URI;
import java.util.List;

/**
 * Controller tipo Rest para la api de exchangerate
 * @Author Carlos Huaman
 */
@RestController
@RequestMapping(value = "/exchangerate")
public class ExchangeRateController {

    @Autowired
    private ExchangeRateService exrService;

    /**
     * Agrega una nueva tasa de cambio en la base de datos
     * @param exchangeRate
     * @return
     */
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Single<ResponseEntity<BaseResponse>> addExchangeRate(
            @RequestBody ExchangeRateDTO exchangeRate
    ){
        return exrService.addExchangeRate(exchangeRate).subscribeOn(Schedulers.io())
                .map(s ->
                        ResponseEntity.created(URI.create("/api/exchangerate" + s)).body(BaseResponse.okWithoutData())
                );
    }

    /**
     * Actualiza una tasa de cambio en la base de datos
     * @param id
     * @param exchangeRate
     * @return
     */
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Single<ResponseEntity<BaseResponse>> updateExchangeRate(
            @PathVariable(value = "id") Integer id,
            @RequestBody ExchangeRateDTO exchangeRate){
        if(exchangeRate.getId()==null){
            exchangeRate.setId(id);
        }
        return exrService.updateRate(exchangeRate).subscribeOn(Schedulers.io())
                .toSingle(() -> ResponseEntity.ok(BaseResponse.okWithoutData()));
    }

    /**
     * Obtiene todas las tasas de cambio de una moneda
     * @param base
     * @return
     */
    @RequestMapping("/allRatesFor")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Single<ResponseEntity<BaseResponse<List<ExchangeRateResponse>>>> getAllRatesFor(
            @RequestParam(value="base", defaultValue = "PEN") String base
    ){
        return exrService.getAllRatesFor(base).subscribeOn(Schedulers.io())
                .map(exrates ->
                        ResponseEntity.ok(BaseResponse.okWithData(exrates))
                );
    }

    /**
     * Obtiene la tasa de cambio para una moneda base y la moneda destino solicitada
     * @param base
     * @param target
     * @return
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Single<ResponseEntity<BaseResponse<ExchangeRateResponse>>> getRateFor(
            @RequestParam(value = "base", defaultValue = "PEN") String base,
            @RequestParam(value = "target", defaultValue = "USD") String target
    ){
        return exrService.getRateFor(base, target).subscribeOn(Schedulers.io())
                .map(exrate ->
                        ResponseEntity.ok(BaseResponse.okWithData(exrate))
                );
    }

    /**
     * Obtiene el valor de cambio de un monto de una moneda a una moneda destino
     * @param amountToChange
     * @return
     */
    @RequestMapping("/getExchangeAmount")
    @GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<BaseResponse<ExchangeAmountDTO>> getExchangeAmount(
            @RequestBody ExchangeAmountDTO amountToChange
    ){

        ExchangeAmountDTO newAmount = null;
        try{
            BigDecimal amountChanged = exrService.doChange(amountToChange.getCurrencyBase(),amountToChange.getCurrencyTarget(),amountToChange.getAmountBase());
            newAmount = new ExchangeAmountDTO();
            BeanUtils.copyProperties(amountToChange,newAmount);
            newAmount.setAmountTarget(amountChanged);

        }catch (Exception e){
            System.out.println("Error haciendo el cambio ");
            e.printStackTrace();

        }
        if(newAmount == null){
            return ResponseEntity.badRequest().body(BaseResponse.error("bad"));
        }
        return ResponseEntity.ok(BaseResponse.okWithoutData());
    }
}
