package com.bcp.exchangerate.datamodel;

public enum ExchangeRateType {
    REAL,
    NOMINAL
}
