package com.bcp.exchangerate.repository;

import com.bcp.exchangerate.datamodel.ExchangeRate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ExchangeRateRepository extends JpaRepository<ExchangeRate,Integer> {

    @Query("select exr from ExchangeRate exr where exr.base = :base")
    public List<ExchangeRate> listAllRatesFor(@Param("base") String base);

    @Query("select exr from ExchangeRate exr where exr.base = :base and exr.target = :target and exr.date >= CURRENT_DATE")
    public ExchangeRate getRateFor(@Param("base") String base, @Param("target") String target);

    @Query("select exr from ExchangeRate exr where exr.base = :base and exr.target = :target and  exr.date >= :date")
    public ExchangeRate getRateFor(@Param("base") String base,@Param("target") String target,@Param("date") LocalDate date);
}
